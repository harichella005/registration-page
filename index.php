<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <title>Registration</title>
</head>

<body>
    <form class="form" action="">
        <div class="container registration" data-aos="flip-left">
            <script>
                AOS.init();
            </script>
            <img class="logo" src="./assets/t.png" alt="Logo">
            <div class="forn-field">
                <h1>Sign Up</h1>
                <div>
                    <label for="">First Name</label>
                    <input type="text" name="firstname" maxlength="50" placeholder="Enter first name" required>
                </div>
                <div>
                    <label for="">Last Name</label>
                    <input type="text" name="lastname" maxlength="50" placeholder="Enter last name" required>
                </div>
                <div>
                    <label for="">Phone Number</label>
                    <input class="phone" pattern="[0-9]{10}" maxlength="10" placeholder="01234 56789">
                </div>
                <div>
                    <label for="email">Email</label>
                    <input type="mail" placeholder="emailid20@gmail.com" required></input>
                </div>
                <script>
                    var check = function() {
                        if (document.getElementById('password').value == 
                        document.getElementById('confirm_password').value) {
                            document.getElementById('message').style.color  = 'green';
                            document.getElementById('message').innerHTML = 'Matched';
                        }else{
                            document.getElementById('message').style.color  = 'red';
                            document.getElementById('message').innerHTML = 'Not Matching';
                        }
                    }
                </script>
                <div>
                    <label for="password">New Password</label>
                    <input type="password" id="password" placeholder="New password" maxlength="25" onkeyup="check();" required>
                </div>
                <div>
                    <label for="password">Confirm Password</label>
                    <input type="password" id="confirm_password" placeholder="Confirm password" maxlength="25" onkeyup="check();" required>
                    <span id="message"></span>
                </div>
                <div>
                    <button onclick="Thanks()" type="submit">Sign Up</button>
                </div>
                <div class="haveaccount">
                    <p>Already have an account? <span><a href="login.php">Login</a></span> </p>
                </div>
            </div>

        </div>
    </form>
    
</body>

</html>