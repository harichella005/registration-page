<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <title>Reset Password</title>
</head>

<body>
    <form class="form" action="">
        <div class="container registration" data-aos="flip-left">
            <script>
                AOS.init();
            </script>
            <img class="logo" src="./assets/t.png" alt="Logo">
            <div class="forn-field">
                <h1>Reset New Password</h1>
                <div>
                    <label for="email">Email</label>
                    <input type="mail" placeholder="emailid20@gmail.com" required></input>
                </div>
                <div>
                    <label for="password">New Password</label>
                    <input type="password" id="password" placeholder="password" maxlength="25" onkeyup="check();"
                        required>
                </div>
                <div>
                    <label for="password">Confirm Password</label>
                    <input type="password" id="confirm_password" placeholder="Confirm password" maxlength="25" onkeyup="check();" required>
                    <span id="message"></span>
                </div>
                <div>
                    <button onclick="Thanks()" type="submit">Login</button>
                </div>
                <div class="haveaccount">
                    <p>Don't have an account? <span><a href="/">Sign Up</a></span> </p>
                </div>
                <div>
                    <p>or</p>

                </div>

            </div>

        </div>
    </form>

</body>

</html>